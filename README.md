# ImportContacts
a script for retrieving contacts from google, twitter etc.

###### Requirements
- google api project specifically for people api (go to google api console for setup)
- developer credentials for twitter






###### Google People API


Fields are predefined but can be override.


(please be guided for requestMask parameters [documentation](https://developers.google.com/people/api/rest/v1/RequestMask))
```sh
    INCLUDE_FIELDS = 'person.names,person.emailAddresses,person.phoneNumbers'
```

##### How to run
Call this function

    importContacts(size) --> returns a list of dict with specificied fields
