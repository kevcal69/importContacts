import json
import httplib2
from apiclient.discovery import build
from oauth2client.file import Storage
from oauth2client.client import OAuth2WebServerFlow, GoogleCredentials
from oauth2client.tools import run_flow

USER_AGENT = 'GetContacts/Test1'
PEOPLE_SCOPE_API = ['https://www.googleapis.com/auth/contacts.readonly',
                    'https://www.googleapis.com/auth/user.emails.read']
INCLUDE_FIELDS = 'person.names,person.emailAddresses,person.phoneNumbers'


def getCredentials(api_name):
    with open('credentials.json') as creds:
        creds = json.loads(creds.read())
    credentials = creds.get(api_name, None)
    if not credentials:
        raise KeyError(
            "%s credentials not found in credentials.json" % api_name)
    return credentials


def getAuthenticationGoogle(credentials):
    FLOW = OAuth2WebServerFlow(
        client_id=credentials['client_id'],
        client_secret=credentials['client_secret'],
        scope=PEOPLE_SCOPE_API,
        user_agent=USER_AGENT)

    storage = Storage('info.dat')
    credentials = storage.get()
    if credentials is None or credentials.invalid is True:
        credentials = run_flow(FLOW, storage)

    http = httplib2.Http()
    return credentials.authorize(http)


def buildService(serviceName, ver):
    credentials = getCredentials('google')
    http = getAuthenticationGoogle(credentials)

    service = build(serviceName=serviceName, version=ver, http=http)
    return service


def importContacts(size=20):
    service = buildService('people', 'v1')
    results = service.\
        people().\
        connections().\
        list(resourceName='people/me',
             pageSize=20,
             requestMask_includeField=INCLUDE_FIELDS).\
        execute()

    connections = results.get('connections', [])
    poeple_list = []

    for person in connections:
        names = person.get('names', '')
        phoneNumbers = person.get('phoneNumbers', '')
        emailAddresses = person.get('emailAddresses', '')
        name = " ".join([name.get('displayName') for name in names])
        email = " ".join(
            [email.get('value') for email in emailAddresses])
        number = " ".join(
            [number.get('value') for number in phoneNumbers])
        poeple_list.append({
            'name': name,
            'email': email,
            'number': number
        })

    return poeple_list


# TODO:
#   * Can use cursor for getting next results
